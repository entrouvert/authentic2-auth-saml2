from django.dispatch import Signal

#authz_decision
authz_decision = Signal(providing_args = ["request","attributes","provider"])

#user login
auth_login = Signal(providing_args = ["request","attributes"])

#user logout
auth_logout =  Signal(providing_args = ["user"])

from authentic2.saml.common import authz_decision_cb

authz_decision.connect(authz_decision_cb,
        dispatch_uid='authz_decision_on_attributes')


