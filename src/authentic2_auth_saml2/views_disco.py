import logging
import urlparse
import urllib

from django.core.urlresolvers import reverse
from django.http import Http404, HttpResponseRedirect
from django.utils.translation import ugettext as _

from .utils import register_next_target, error_page, get_registered_url
from . import app_settings

logger = logging.getLogger(__name__)


def get_return_id_param():
    return app_settings.DISCO_RETURN_ID_PARAM

##############################################################
#
# Discovery service Requester
# See Identity Provider Discovery Service Protocol and Profile
# OASIS Committee Specification 01
# 27 March 2008
#
##############################################################
def build_discovery_url(request, target):
    '''Build the URL for redirection to the disctovery service'''
    scheme, netloc, path, params, query, fragment = urlparse.urlparse(target)
    # Mix query strings
    d = urlparse.parse_qs(query)
    d.update({
        'entityID': request.build_absolute_uri(reverse('a2-auth-saml2-metadata')),
        'return': request.build_absolute_uri(reverse('a2-auth-saml2-disco-response')),
        'returnIDParam': get_return_id_param(),
    })
    query = urllib.urlencode(d)
    return urlparse.urlunparse((scheme, netloc, path, params, query, fragment))


def redirect_to_disco(request):
    '''Send a discovery request to the default disco service'''
    if not app_settings.DISCO_SERVICE_NAME:
        raise Http404
    register_next_target(request)
    try:
        target = app_settings.DISCO_SERVICE_NAME
    except:
        logger.error('missing parameter in settings')
        return None
    url = build_discovery_url(request, target)
    if not url:
        msg = _('redirect_to_disco: unable to build disco request')
        return error_page(request, msg, logger=logger)
    return HttpResponseRedirect(url)


def disco_response(request):
    '''Handle the discovery response'''
    if not app_settings.DISCO_SERVICE_NAME:
        raise Http404
    if not request.method == "GET":
        message = _('HTTP request not supported')
        return error_page(request, message, logger=logger)
    provider = request.GET.get(get_return_id_param(), '')
    if provider:
        request.session['prefered_idp'] = provider
        logger.debug('discovered %s', provider)
    else:
        logger.debug('no provider discovered')
    return HttpResponseRedirect(get_registered_url(request))


