from django.conf.urls import patterns, url

urlpatterns = patterns('authentic2_auth_saml2.views',
    url(r'^metadata$', 'metadata', name='a2-auth-saml2-metadata'),
    # Receive request from user interface
    url(r'^sso/$', 'sso', name='a2-auth-saml2-sso'),
    url(r'^account-linking/(?P<response_id>.*)/$', 'account_linking',
        name='a2-auth-saml2-account-linking'),
    url(r'^singleSignOnArtifact$', 'assertion_consumer_artifact'),
    url(r'^singleSignOnPost$', 'assertion_consumer_post'),
    # Receive request from functions
    url(r'^sp_slo/$', 'sp_slo', name='a2-auth-saml2-slo'),
    # Receive response from Redirect SP initiated
    url(r'^singleLogoutReturn$', 'slo_sp_response'),
    # Receive request from SOAP IdP initiated
    url(r'^singleLogoutSOAP$', 'slo_soap'),
    # Receive request from Redirect IdP initiated
    url(r'^singleLogout$', 'singleLogout'),
    # Back of SLO treatment by the IdP Side
    url(r'^finish_slo$', 'finish_slo',
        name='a2-auth-saml2-finish-slo'),
)

urlpatterns += patterns('authentic2_auth_saml2.views_disco',
    url(r'^redirect_to_disco/$', 'redirect_to_disco',
        name='a2-auth-saml2-redirect-to-disco'),
    #receive idp discovery response
    url(r'^discoveryReturn/$', 'disco_response',
        name='a2-auth-saml2-disco-response'),
)

urlpatterns += patterns('authentic2_auth_saml2.views_profile',
    url(r'^delete_federation/(?P<provider_id>\d+)/$', 'delete_federation',
        name='a2-auth-saml2-delete-federation'),
)
