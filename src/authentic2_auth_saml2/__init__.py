
__version__ = '1.0'

class Plugin(object):
    def init(self):
        from authentic2.decorators import TRANSIENT_USER_TYPES
        from . import transient

        TRANSIENT_USER_TYPES.append(transient.SAML2TransientUser)

    def get_before_urls(self):
        from django.conf.urls import patterns, url, include
        return patterns('',
                url('^authsaml/', include('authentic2_auth_saml2.urls')))

    def get_apps(self):
        return [__name__, 'authentic2.saml']

    def get_authentication_backends(self):
        return (
                'authentic2_auth_saml2.backends.AuthSAML2PersistentBackend',
                'authentic2_auth_saml2.backends.AuthSAML2TransientBackend',
        )

    def get_auth_frontends(self):
        return ('authentic2_auth_saml2.frontend.AuthSAML2Frontend',)

    def get_idp_backends(self):
        return ('authentic2_auth_saml2.backends.AuthSAML2Backend',)

    def logout_list(self, request):
        return []

    def get_saml2_authn_context(self, backend_cls):
        import lasso

        if backend_cls.startswith('authentic2_auth_saml2.'):
            return lasso.SAML2_AUTHN_CONTEXT_UNSPECIFIED
        return None
