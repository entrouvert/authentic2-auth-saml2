class AppSettings(object):
    __DEFAULTS = {
            'METADATA_OPTIONS': {},
            'DISCO_RETURN_ID_PARAM': 'entityID',
            'DISCO_SERVICE_NAME': None,
    }

    def __init__(self, prefix):
        self.prefix = prefix

    def SHOW_DISCO_IN_MD(self):
        from django.conf import settings
        return getattr(settings, 'SHOW_DISCO_IN_MD', False)

    def _setting(self, name, dflt):
        from django.conf import settings
        return getattr(settings, self.prefix+name, dflt)

    def __getattr__(self, name):
        if name in self.__DEFAULTS:
            return self._setting(name, self.__DEFAULTS[name])
        raise AttributeError(name)


# Ugly? Guido recommends this himself ...
# http://mail.python.org/pipermail/python-ideas/2012-May/014969.html
import sys
app_settings = AppSettings('A2_AUTH_SAML2_')
app_settings.__name__ = __name__
sys.modules[__name__] = app_settings
