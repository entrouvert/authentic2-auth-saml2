from django import forms
from django.utils.translation import ugettext as _


from authentic2.utils import IterableFactory
from authentic2.saml import models


def provider_list_to_choices(qs):
    for idp in qs:
        yield idp.entity_id, idp.name

def get_idp_list():
    qs = models.LibertyProvider.objects.idp_enabled().order_by('name')
    return provider_list_to_choices(qs)

class AuthSAML2Form(forms.Form):
    entity_id = forms.ChoiceField(label=_('Choose your identity provider'),
            choices=IterableFactory(get_idp_list))
