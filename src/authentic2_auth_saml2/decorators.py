from django.conf import settings
from django.utils.http import urlencode
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.http import HttpResponseRedirect

from functools import wraps

def anonymous_only(func):
    '''Logout before entering this view'''
    @wraps(func)
    def f(request, *args, **kwargs):
        if request.user.is_authenticated():
            current_url = request.build_absolute_uri()
            query = urlencode({REDIRECT_FIELD_NAME: current_url})
            logout_url = '{0}?{1}'.format(settings.LOGOUT_URL, query)
            return HttpResponseRedirect(logout_url)
        return func(request, *args, **kwargs)
    return f


