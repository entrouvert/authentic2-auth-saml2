import logging

from django.template.loader import render_to_string
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.utils.translation import ugettext as _
from django.shortcuts import redirect
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.decorators import login_required
from django.contrib import messages

from authentic2.saml.models import LibertyProvider, LibertyFederation

from . import forms

logger = logging.getLogger(__name__)

def profile(request, template_name='authsaml2/profile.html'):
    linked_providers = LibertyProvider.objects.with_federation(request.user)
    unlinked_providers = LibertyProvider.objects.idp_enabled() \
            .without_federation(request.user)
    choices = list(forms.provider_list_to_choices(unlinked_providers))

    form = forms.AuthSAML2Form()
    if not choices:
        form = None
    else:
        form.fields['entity_id'].choices = choices
    context = {'submit_name': 'submit-auhthsaml2',
                REDIRECT_FIELD_NAME: '/profile',
                'form': form,
                'next': next,
                'linked_providers': linked_providers,
                'base': '/authsaml2',
    }
    return render_to_string(template_name, context, RequestContext(request))


@login_required
@csrf_exempt
def delete_federation(request, provider_id):
    '''Delete federations with the passed provider'''
    if request.method == "POST":
        qs = LibertyFederation.objects.filter(user=request.user,
                idp__liberty_provider__id=provider_id)
        federations = list(qs)
        qs.update(user=None)
        for federation in federations:
            logger.info('federation %s deleted', federation.id)
        msg = _('Successful federation deletion.')
        messages.add_message(request, messages.INFO, msg)
    return redirect('account_management')
