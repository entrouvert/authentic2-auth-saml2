import urllib

from django.utils.translation import gettext_noop
from django.http import HttpResponseRedirect
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.core.urlresolvers import reverse

from authentic2.saml.models import LibertyProvider

from . import forms, views_profile

class AuthSAML2Frontend(object):
    def enabled(self):
        return LibertyProvider.objects.idp_enabled().exists()

    def id(self):
        return 'saml2'

    def name(self):
        return gettext_noop('SAML 2.0')

    def form(self):
        return forms.AuthSAML2Form

    def post(self, request, form, nonce, next_url):
        entity_id = form.cleaned_data['entity_id']
        query = urllib.urlencode({
            'entity_id': entity_id,
            REDIRECT_FIELD_NAME: next_url,
        })
        url = '{0}?{1}'.format(reverse('a2-auth-saml2-sso'), query)
        return HttpResponseRedirect(url)

    def get_context(self):
        '''Specific context variable used by the specific template'''
        qs = LibertyProvider.objects.idp_enabled().order_by('name')
        choices = forms.provider_list_to_choices(qs)
        return { 'idp_providers': choices }

    def template(self):
        return 'authsaml2/login_form.html'

    def profile(self, request):
        return views_profile.profile(request)
