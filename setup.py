#!/usr/bin/python
from setuptools import setup, find_packages
import os

def get_version():
    import glob
    import re
    import os

    version = None
    for d in glob.glob('src/*'):
        if not os.path.isdir(d):
            continue
        module_file = os.path.join(d, '__init__.py')
        if not os.path.exists(module_file):
            continue
        for v in re.findall("""__version__ *= *['"](.*)['"]""",
                open(module_file).read()):
            assert version is None
            version = v
        if version:
            break
    assert version is not None
    if os.path.exists('.git'):
        import subprocess
        p = subprocess.Popen(['git','describe','--dirty','--match=v*'],
                stdout=subprocess.PIPE)
        result = p.communicate()[0]
        assert p.returncode == 0, 'git returned non-zero'
        new_version = result.split()[0][1:]
        assert new_version.split('-')[0] == version, '__version__ must match the last git annotated tag'
        version = new_version.replace('-', '.')
    return version

setup(name='authentic2-auth-saml2',
        version=get_version(),
        license='AGPLv3',
        description='Authentic2 Auth SAML2',
        author="Entr'ouvert",
        author_email="info@entrouvert.com",
        packages=find_packages('src'),
        package_dir={
            '': 'src',
        },
        install_requires=[
            'django-mellon',
        ],
        package_data={
            'authentic2_auth_saml2': [
                  'templates/authsaml2/*.html',
                  'locale/fr/LC_MESSAGES/django.po',
                  'locale/fr/LC_MESSAGES/django.mo',
                  'static/authentic2_auth_kerberos/js/*.js',
                  'static/authentic2_auth_kerberos/css/*.css',
                  'static/authentic2_auth_kerberos/img/*.png',
            ],
        },
        entry_points={
            'authentic2.plugin': [
                'authentic-auth-saml2 = authentic2_auth_saml2:Plugin',
            ],
        },
)
